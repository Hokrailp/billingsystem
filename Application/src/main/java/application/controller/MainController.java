package application.controller;

import application.entity.Customer;
import application.service.BillingService;
import application.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class MainController {
    //TODO
    @Autowired
    BillingService billingService;
    @Autowired
    CustomerService customerService;

    @GetMapping("/user/me")
    public Principal user(Principal principal) {
        return principal;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getGreeting(@RequestParam(name = "hello",required = false,defaultValue = "Application") String msg) {
        return "Hello from " + msg + "!";
    }

    @RequestMapping(value = "/billing", method = RequestMethod.GET)
    public String getGreetingFromBE() {
        return billingService.getServiseName();
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public Collection<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    private String addCustomers() {
        List<Customer> customers = new ArrayList<Customer>(){{
            add(new Customer("Alex", "Lion", 191));
            add(new Customer("Tommy", "Cash", 292));
            add(new Customer("Timmy", "Terner", 911));
            add(new Customer("Mike", "Vazovski", 112));
        }};
        for (Customer i : customers) {
            customerService.save(i);
        }
        return "added";
    }


}
