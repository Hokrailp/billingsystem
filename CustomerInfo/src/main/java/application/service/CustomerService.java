package application.service;

import application.entity.Customer;

import java.util.Collection;

public interface CustomerService {
    //TODO
    Collection<Customer> getAllCustomers();

    void save(Customer customer);
}
