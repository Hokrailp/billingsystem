package application.service;

import application.database.Factory;
import application.entity.Customer;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Override
    public Collection<Customer> getAllCustomers() {
        try {
            return Factory.getInstance().getCustomerDAO().getAllCustomers();
        } catch (SQLException exc) {
            return null;
        }
    }

    @Override
    public void save(Customer customer) {
        try {
            Factory.getInstance().getCustomerDAO().addCustomer(customer);
        } catch (SQLException exc) {}
    }
}
