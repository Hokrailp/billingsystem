package application.database;

import application.entity.Customer;

import java.sql.SQLException;
import java.util.Collection;

public interface CustomerDAO {
    void addCustomer(Customer customer) throws SQLException;

    void updateCustomer(Long customer_id, Customer customer) throws SQLException;

    Customer getCustomerById(Long customer_id) throws SQLException;

    Collection<Customer> getAllCustomers() throws SQLException;

    void deleteCustomer(Customer customer) throws SQLException;
}
